// Program 1a: Changeback
// Author: Kelly Morales
// Description: This program calculates and prints the breakdown of change to
// be given after a transaction.

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main()
{
  cout.setf(ios::showpoint);
  cout.setf(ios::fixed);
  cout.precision(2);
  int a1, a2, t1, t2;
  char dot;
  int count = 0;
  int dollarValues[] = {50, 20, 10, 5, 1};
  int centValues[] = {50, 25, 10, 5, 1};
  while (true){ //reads from stdin
    cin >> a1 >> dot >> a2;
    int amt = a1 * 100 + a2; // avoids truncation error
    cin >> t1 >> dot >> t2;
    int tend = t1 * 100 + t2;
    if (amt == 0 && tend == 0) return 0;
    int dollar = t1 - a1;
    if (t2 - a2 < 0){
      t2 += 100;
      dollar -= 1;
    }
    int cents = t2 - a2;
    stringstream ss;
    ss << "$" << dollar << "." << cents;
    string change = ss.str();
    cout << "TRANSACTION #" << ++count << ":\n";
    cout << change << endl;
    int dollarNum = 0;
    int centsNum = 0;
    for (int i = 0; i < 5; i++){ //dollar amounts
      if (dollar > 0 && dollar >= dollarValues[i]){
        dollarNum = dollar / dollarValues[i]; //number of this bill used
        dollar -= dollarNum * dollarValues[i];
	if (dollarNum > 0){
	  cout << dollarNum << " $" << dollarValues[i] << " bill";
	  if (dollarNum > 1)
	    cout << 's';
	  
	  cout << endl;
	}	
      }
    }
    for (int i = 0; i < 5; i++){ //cent amounts
      if (cents > 0 && cents >= centValues[i]){
        centsNum = cents / centValues[i];
        cents -= centsNum * centValues[i];
	if (centsNum > 0){
	  cout << centsNum << " " << centValues[i] << "-cent coin";
	  if (centsNum > 1)
	    cout << 's';
          cout << endl;
	}	
      }
    }
    cout << endl;
  }
  return 0;
}
